<?php

$player = $argv[2];
$url = "http://tictactoe.homedir.eu/game/{$argv[1]}/player/" . ($player == 'x' ? 1 : 2);
$repeatResponse = 3;

$moves = [];
//$moves = json_decode('{"0":{"x":0,"y":1,"v":"x"}}', true);
//print_r($moves);

play($url, $player, $moves);

function getBlock(array $moves = [], $block, $index = 0)
{
    if ($block == 'row') {
        return array_values(array_filter($moves, function ($move) use ($index) {
            return $move['x'] == $index;
        }));
    } else if ($block == 'col') {
        return array_values(array_filter($moves, function ($move) use ($index) {
            return $move['y'] == $index;
        }));
    } else if ($block == 'diag') {
        return array_values(array_filter($moves, function ($move) use ($index) {
            if ($index == 0) {
                return (($move['x'] == 0) && ($move['y'] == 0)) || (($move['x'] == 1) && ($move['y'] == 1)) || (($move['x'] == 2) && ($move['y'] == 2));
            } else {
                return (($move['x'] == 0) && ($move['y'] == 2)) || (($move['x'] == 1) && ($move['y'] == 1)) || (($move['x'] == 2) && ($move['y'] == 0));
            }
        }));
    }
}

function getWinner(array $moves = [], $block = 'row', $index = 0)
{
    if (($block == 'row') && ($index == 3)) {
        return getWinner($moves, 'col', 0);
    }

    if (($block == 'col') && ($index == 3)) {
        return getWinner($moves, 'diag', 0);
    }

    if (($block == 'diag') && ($index == 2)) {
        return (count($moves) == 9)
            ? 'eq'
            : '-'
        ;
    }

    if ((3 == count(getBlock($moves, $block, $index))) && (1 == count(array_unique(array_column(getBlock($moves, $block, $index), 'v'))))) {
        return getBlock($moves, $block, $index)[0]['v'];
    } else {
        return getWinner($moves, $block, $index + 1);
    }
}

function printWinner($winner)
{
    if (in_array($winner, ['x', 'o'])) {
        echo 'Winner is ' . $winner . '!' . PHP_EOL;
    } else if ($winner == 'eq') {
        echo 'Draw!' . PHP_EOL;
    }
}

function isMyTurn(array $moves = [], $player)
{
    if (!empty($moves)) {
        return end($moves)['v'] != $player;
    }

    return $player == 'x';
}

function isMove($data)
{
    return
        is_array($data) &&
        isset($data['x']) && ($data['x'] > -1)  &&
        isset($data['y']) && ($data['y'] > -1) &&
        isset($data['v'])
    ;
}

function isEmptyCell(array $moves = [], $x, $y)
{
    return empty(array_filter($moves, function ($move) use ($x, $y) {
        return ($move['x'] == $x) && ($move['y'] == $y);
    }));
}

function play($url, $player, array $moves = [], $repeat = 2)
{
    if ($repeat >= 0) {
        if (isMyTurn($moves, $player)) {
            $move = generateBestMove($moves, $player);
            print_r($move);

            if (isMove($move)) {
                $newMoves = addNewMove($moves, $move);
                sendMove($url, $newMoves);

                $winner = getWinner($newMoves);

                if ($winner == '-') {
                    play($url, $player, $newMoves);
                } else {
                    printWinner($winner);
                }
            }
        } else {
            $response = receiveOpponentMove($url);

            if (isMove($response)) {
                $newMoves = addNewMove($moves, $response);
                $winner = getWinner($newMoves);

                if ($winner == '-') {
                    play($url, $player, $newMoves);
                } else {
                    printWinner($winner);
                }
            } else {
                play($url, $player, $moves, $repeat - 1);
            }
        }
    }
}

function addNewMove(array $moves, $move)
{
    if (isMove($move)) {
        return array_merge($moves, [$move]);
    }

    return $moves;
}

function encodeMoves(array $moves = [], $index = 0)
{
    if (count($moves)) {
        $encoded = "\"{$index}\":" . json_encode($moves[0]);
        array_shift($moves);

        if (count($moves)) {
            $encoded .= ', ' . encodeMoves($moves, $index + 1);
        }

        return $encoded;
    }

    return '';
}

function sendMove($url, array $moves = [])
{
    $encoded = '{' . encodeMoves($moves) . '}';

    $context = stream_context_create(
        array(
            'http' => array(
                'method' => 'POST',
                'header' =>
                    'Content-Type: application/json+map' . "\r\n" .
                    'Content-Length: ' . strlen($encoded) . "\r\n",
                'content' => $encoded,
            ),
        )
    );

    print_r("POST " . $encoded . PHP_EOL);

    return @file_get_contents($url, null, $context);
}

function receiveOpponentMove($url)
{
    $context = stream_context_create(
        array(
            'http' => array(
                'method' => 'GET',
                'header' => 'Accept: application/json+map',
            ),
        )
    );

    print_r("GET" . PHP_EOL);

    $response = @file_get_contents($url, null, $context);

    $data = json_decode($response, true);

    if (is_array($data) && isMove(end($data))) {
        return end($data);
    }

    return '';
}

function isWinnerGameAttackPossible(array $moves = [], $block = 'row', $index = 0, $player)
{
    return isDefendNeeded($moves, $block, $index) && in_array($player, array_column(getBlock($moves, $block, $index), 'v'));
}

function isDefendNeeded(array $moves = [], $block = 'row', $index = 0)
{
    return ((2 == count(getBlock($moves, $block, $index))) && (1 == count(array_unique(array_column(getBlock($moves, $block, $index), 'v')))));
}

function getLastMoveForBlock($moves, $block = 'row', $index = 0)
{
    $blockMoves = getBlock($moves, $block, $index);

    if ($block == 'row') {
        $x = $index;
        $y = end(array_diff([0, 1, 2], array_column($blockMoves, 'y')));
    } else if ($block == 'col') {
        $x = end(array_diff([0, 1, 2], array_column($blockMoves, 'x')));
        $y = $index;
    } else if (($block == 'diag') && ($index == 0)) {
        if (isEmptyCell($blockMoves, 0, 0)) {
            $x = 0;
            $y = 0;
        } else if (isEmptyCell($blockMoves, 2, 2)) {
            $x = 2;
            $y = 2;
        } else if (isEmptyCell($blockMoves, 1, 1)) {
            $x = 1;
            $y = 1;
        }
    } else if (($block == 'diag') && ($index == 1)) {
        if (isEmptyCell($blockMoves, 0, 2)) {
            $x = 0;
            $y = 2;
        } else if (isEmptyCell($blockMoves, 2, 0)) {
            $x = 2;
            $y = 0;
        } else if (isEmptyCell($blockMoves, 1, 1)) {
            $x = 1;
            $y = 1;
        }
    } else {
        $x = -1;
        $y = -1;
    }

    return [
        'x' => $x,
        'y' => $y,
    ];
}

function getLastMoveOfFirstAvailableBlock(array $moves = [], $player, $block = 'row', $index = 0, $moveType = 'gameWinner')
{
    if (($block == 'row') && ($index == 3)) {
        return getLastMoveOfFirstAvailableBlock($moves, $player, 'col', 0, $moveType);
    }

    if (($block == 'col') && ($index == 3)) {
        return getLastMoveOfFirstAvailableBlock($moves, $player, 'diag', 0, $moveType);
    }

    if (($block == 'diag') && ($index == 2)) {
        return '';
    }

    $function = ($moveType == 'gameWinner') ? 'isWinnerGameAttackPossible' : 'isDefendNeeded';

    if ($function($moves, $block, $index, $player)) {
        return array_merge(getLastMoveForBlock($moves, $block, $index), ['v' => $player]);
    } else {
        return getLastMoveOfFirstAvailableBlock($moves, $player, $block, $index + 1, $moveType);
    }
}

function getOppositeCorner($move)
{
    $x = $move['x'] == 0 ? 2 : 0;
    $y = $move['y'] == 0 ? 2 : 0;

    return [
        'x' => $x,
        'y' => $y,
    ];
}

function isCorner($move)
{
    $coords = [$move['x'], $move['y']];

    return
        ([0, 0] == $coords) ||
        ([0, 2] == $coords) ||
        ([2, 0] == $coords) ||
        ([2, 2] == $coords)
    ;
}

function getOccupiedCorners(array $moves, $x = 0, $y = 0)
{
    if ($x > 2) {
        return [];
    }

    if ($y > 2) {
        return getOccupiedCorners($moves, $x + 1, 0);
    }

    if (isCorner(['x' => $x, 'y' => $y]) && !isEmptyCell($moves, $x, $y)) {
        return array_merge([[$x, $y]], getOccupiedCorners($moves, $x, $y + 1));
    } else {
        return getOccupiedCorners($moves, $x, $y + 1);
    }
}

function getMove(array $moves = [], $player, $moveType = 'gameWinner')
{
    if ($moveType == 'gameWinner') {
        return getLastMoveOfFirstAvailableBlock($moves, $player, 'row', 0, $moveType);
    } else if ($moveType == 'defend') {
        return getLastMoveOfFirstAvailableBlock($moves, $player, 'row', 0, $moveType);
    }

    return '';
}

function getEmptyCell(array $moves = [], $moveType = 'side', $x = 0, $y = 0)
{
    if ($x > 2) {
        return [];
    }

    if ($y > 2) {
        return getEmptyCell($moves, $moveType, $x + 1, 0);
    }

    if (isEmptyCell($moves, $x, $y)) {
        if (
            ($moveType == 'firstEmpty') ||
            ($moveType == 'side' && ([1, 1] != [$x, $y]) && !isCorner(['x' => $x, 'y' => $y])) ||
            ($moveType == 'corner') && isCorner(['x' => $x, 'y' => $y])
        ) {
            return [$x, $y];
        }
    }

    return getEmptyCell($moves, $moveType, $x, $y + 1);
}

function generateBestMove(array $moves = [], $player)
{
    if (empty($moves)) {
        if (rand(1, 5) == 1) {
            return [
                'x' => 1,
                'y' => 1,
                'v' => $player,
            ];
        }

        return [
            'x' => [0, 2][rand(0, 1)],
            'y' => [0, 2][rand(0, 1)],
            'v' => $player
        ];
    }

    if (isMove(getMove($moves, $player, 'gameWinner'))) {
        return getMove($moves, $player, 'gameWinner');
    }

    if (isMove(getMove($moves, $player, 'defend'))) {
        return getMove($moves, $player, 'defend');
    }

    if (2 == count($moves) && isCorner($moves[0]) && (getOppositeCorner($moves[0]) != $moves[1])) {
        return array_merge(getOppositeCorner($moves[0]), ['v' => $player]);
    }

    if (isEmptyCell($moves, 1, 1) && ((count($moves) == 1) || (1 < getOccupiedCorners($moves)))) {
        return [
            'x' => 1,
            'y' => 1,
            'v' => $player
        ];
    }

    if (2 == count($moves) && isCorner($moves[0]) && [1, 1] == [$moves[1]['x'], $moves[1]['y']]) {
        return array_merge(getOppositeCorner($moves[0]), ['v' => $player]);
    }

    if (3 == count($moves) && (2 == count(getOccupiedCorners($moves))) && !isEmptyCell($moves, 1, 1)) {
        $sideCell = getEmptyCell($moves, 'side');
        return [
            'x' => $sideCell[0],
            'y' => $sideCell[1],
            'v' => $player
        ];
    }

    if (!empty(getEmptyCell($moves, 'corner'))) {
        $cell = getEmptyCell($moves, 'corner');
        return [
            'x' => $cell[0],
            'y' => $cell[1],
            'v' => $player
        ];
    }

    if (!empty(getEmptyCell($moves, 'firstEmpty'))) {
        $cell = getEmptyCell($moves, 'firstEmpty');

        return [
            'x' => $cell[0],
            'y' => $cell[1],
            'v' => $player
        ];
    }

    return [
        'x' => -1,
        'y' => -1,
        'v' => $player
    ];
}